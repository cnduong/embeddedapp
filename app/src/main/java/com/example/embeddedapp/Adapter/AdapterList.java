package com.example.embeddedapp.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.example.embeddedapp.Model.ChatBotModel.ListModel;
import com.example.embeddedapp.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterList extends RecyclerView.Adapter<AdapterList.ViewHolder> {
    Context context;
    ArrayList<ListModel>listModels;

    public AdapterList(Context context, ArrayList<ListModel> listModels) {
        this.context = context;
        this.listModels = listModels;
    }

    @NonNull
    @Override
    public AdapterList.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(context);
        View view=layoutInflater.inflate(R.layout.item_list_model,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterList.ViewHolder holder, int position) {
        ListModel model=listModels.get(position);


        holder.tvName.setText(model.getBody().getListBody().getNameModel().getName());
        holder.tvName.setTextColor(Color.parseColor(model.getBody().getListBody().getNameModel().getColor()));
        holder.tvName.setTextSize(TypedValue.COMPLEX_UNIT_SP,model.getBody().getListBody().getNameModel().getSize());
        holder.tvMessage.setText(model.getBody().getListBody().getMessageModel().getMessage());
        holder.tvMessage.setTextColor(Color.parseColor(model.getBody().getListBody().getMessageModel().getColor()));
        holder.tvMessage.setTextSize(TypedValue.COMPLEX_UNIT_SP,model.getBody().getListBody().getMessageModel().getSize());
        holder.tvDescription.setText(model.getBody().getDescription().getTitle());
        holder.tvDescription.setTextColor(Color.parseColor(model.getBody().getDescription().getColor()));
        holder.tvDescription.setTextSize(TypedValue.COMPLEX_UNIT_SP,model.getBody().getDescription().getTextSize());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  Toast.makeText(context,model.getBody().getListBody().getNameModel().getName(),Toast.LENGTH_SHORT).show();
            }
        });

        if(model.getBody().getListBody().getImageView()!=null) Glide.with(context).load(model.getBody().getListBody().getImageView()).into(holder.imgModel);

    }

    @Override
    public int getItemCount() {
        return listModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        CircleImageView imgModel;
        TextView tvName;
        TextView tvMessage;

        TextView tvDescription;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgModel=itemView.findViewById(R.id.imgModel);
            tvName=itemView.findViewById(R.id.tvName);
            tvMessage=itemView.findViewById(R.id.tvMessage);

            tvDescription=itemView.findViewById(R.id.tvDescription);
        }
    }
}
