package com.example.embeddedapp.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.embeddedapp.Model.ChatBotModel.BaseModel;
import com.example.embeddedapp.Model.ChatBotModel.LineChartModel;
import com.example.embeddedapp.Model.ChatBotModel.ListModel;
import com.example.embeddedapp.Model.ChatBotModel.Model;
import com.example.embeddedapp.Model.ChatBotModel.PieChartModel;
import com.example.embeddedapp.R;
import com.example.embeddedapp.viewholder.BarChartVH;
import com.example.embeddedapp.viewholder.LineChartVH;
import com.example.embeddedapp.viewholder.ListVH;
import com.example.embeddedapp.viewholder.MessageBotVH;
import com.example.embeddedapp.viewholder.MessageUserVH;
import com.example.embeddedapp.viewholder.PieChartVH;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;

import java.util.ArrayList;

public class AdapterBot extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<Model> models;
    public static final int LIST=1;
    public static final int TABLE=2;
    public static final int LINECHART=3;
    public static final int TEXT=4;
    public static final int BOT=5;
    public static final int USER=6;
    public static final int PIECHART=7;
    public static final int BARCHART=8;
    public int []keangNam= {60,80,100,50,55,65,72,74,80,90,96,86};
    public int []thaiBinh ={10,20,30,40,25,15,22,44,60,120,46,16};
    public AdapterBot(Context context, ArrayList<Model> models) {
        this.context = context;
        this.models = models;
    }

    @Override
    public int getItemViewType(int position) {
        String type=models.get(position).getType();
        String dataType=models.get(position).getData_type();
        int textType = -1;
        if(models.get(position).getChatBotModels()!=null&&models.get(position).getChatBotModels().size()!=0) textType=models.get(position).getChatBotModels().get(0).getType();
        switch (type){
            case "list":{
                return  LIST;

            }
            case "table":{
                return  TABLE;

            }

            case "chart":{
                if(dataType.equalsIgnoreCase("piechart")){
                    return PIECHART;
                }
                else if(dataType.equalsIgnoreCase("barchart")){
                    return BARCHART;
                }
                else {
                    return LINECHART;
                }
            }
            case "text":{
                if(textType==0){
                    return BOT;
                }
                else if(textType ==3) return LINECHART;
                return USER;
            }
        }
        return -1;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater= LayoutInflater.from(parent.getContext());
        View view;
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType){
            case LIST:{
                view =layoutInflater.inflate(R.layout.type_list,parent,false);
                viewHolder=new ListVH(view);
                break;
            }
            case BOT:{
                view =layoutInflater.inflate(R.layout.item_message_from_bot,parent,false);
                viewHolder=new MessageBotVH(view);
                break;
            }
            case LINECHART:{
                view =layoutInflater.inflate(R.layout.item_line_chart,parent,false);
                viewHolder=new LineChartVH(view);
                break;
            }
            case USER:{
                view =layoutInflater.inflate(R.layout.item_message_from_user,parent,false);
                viewHolder=new MessageUserVH(view);
                break;
            }
            case PIECHART:{
                view=layoutInflater.inflate(R.layout.item_pie_chart,parent,false);
                viewHolder=new PieChartVH(view);
                break;
            }
            case BARCHART:{
                view=layoutInflater.inflate(R.layout.item_bar_chart,parent,false);
                viewHolder=new BarChartVH(view);
                break;
            }

        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        String viewType=models.get(position).getType();
        ArrayList<BaseModel> baseModels= models.get(position).getBaseModels();
        if(viewType.equalsIgnoreCase("list")){
            ((ListVH) holder).tvHeader.setText(models.get(position).getTitle());
            ArrayList<ListModel> listModels=new ArrayList<>();

            for(BaseModel m : baseModels){
                ListModel lm=new ListModel(m.getLabel(),m.getValue(),m.getValues(),m.getBody());
                listModels.add(lm);
            }
            AdapterList adapterList=new AdapterList(context,listModels);
            adapterList.notifyDataSetChanged();
            ((ListVH) holder).rclListDanhSach.setAdapter(adapterList);
            LinearLayoutManager linearLayoutManager=new LinearLayoutManager(context);
            linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
            ((ListVH) holder).rclListDanhSach.setLayoutManager(linearLayoutManager);
        }
        else if(viewType.equals("text")){
            if(models.get(position).getData_type().equals("bot")){
                ((MessageBotVH) holder).tvBotMessage.setText(models.get(position).getChatBotModels().get(0).getText());
            }
            else  if(models.get(position).getData_type().equals("chart")){
//                ((MessageBotVH) holder).tvBotMessage.setText(models.get(position).getChatBotModels().get(0).getText());
//                ((MessageBotVH) holder).lineChart.setVisibility(View.VISIBLE);
//                ((MessageBotVH) holder).lineChart.getDescription().setEnabled(false);
//                XAxis xAxis = ((MessageBotVH) holder).lineChart.getXAxis();
//
//                xAxis.setTextSize(11f);
//                xAxis.setTextColor(Color.BLACK);
//                xAxis.setDrawGridLines(false);
//                xAxis.setDrawAxisLine(false);
//                xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//                ((MessageBotVH) holder).lineChart.setData(getDataForChart());
//
//                YAxis rightAxit= ((MessageBotVH) holder).lineChart.getAxisRight();
//                rightAxit.setEnabled(false);
//                rightAxit.setEnabled(false);
            }
            else{
                ((MessageUserVH) holder).tvUserMessage.setText(models.get(position).getChatBotModels().get(0).getText());
            }
        }
        else if(viewType.equals("chart")){
            if(models.get(position).getData_type().equalsIgnoreCase("PieChart")){
                ((PieChartVH) holder).tvBotMessage.setText(models.get(position).getTitle());
                ((PieChartVH) holder).pieChart.setVisibility(View.VISIBLE);
                ArrayList<PieChartModel> pieChartModels=new ArrayList<>();

                for(BaseModel m : baseModels){

                    PieChartModel  pmChart=new PieChartModel(m.getLabel(),m.getValue(),m.getValues(),m.getBody());
                    pieChartModels.add(pmChart);
                }
                ArrayList<PieEntry> pieEntries=new ArrayList<>();

                for(int i=0;i<pieChartModels.size();i++){
                    pieEntries.add(new PieEntry(pieChartModels.get(i).getValue(),pieChartModels.get(i).getLabel()));

                }
                PieDataSet pieDataSet=new PieDataSet(pieEntries,"");
                pieDataSet.setDrawIcons(false);
                pieDataSet.setSliceSpace(3f);// khoang cach theo tuc phan
                pieDataSet.setIconsOffset(new MPPointF(0, 40));
                pieDataSet.setSelectionShift(1f); //do co vao cua hinh tron
                ArrayList<Integer> colors = new ArrayList<>();

                for (int c : ColorTemplate.VORDIPLOM_COLORS)
                    colors.add(c);

                for (int c : ColorTemplate.JOYFUL_COLORS)
                    colors.add(c);

                for (int c : ColorTemplate.COLORFUL_COLORS)
                    colors.add(c);

                for (int c : ColorTemplate.LIBERTY_COLORS)
                    colors.add(c);

                for (int c : ColorTemplate.PASTEL_COLORS)
                    colors.add(c);
                colors.add(ColorTemplate.getHoloBlue());//add ma mau

                pieDataSet.setColors(colors);
                PieData data = new PieData(pieDataSet);
                data.setValueFormatter(new PercentFormatter( ((PieChartVH) holder).pieChart));
                data.setValueTextSize(8f);//textsize
                data.setValueTextColor(Color.BLACK);//textColor
            //    data.setValueTypeface(tfLight);//fontchu
                ((PieChartVH) holder).pieChart.setData(data);

                // undo all highlights
                ((PieChartVH) holder).pieChart.highlightValues(null);

                ((PieChartVH) holder).pieChart.invalidate();
            }
            else if(models.get(position).getData_type().equals("barchart")){
                ((BarChartVH)holder).tvBotMessage.setText(models.get(position).getHeader());

            }
            else if(models.get(position).getData_type().equalsIgnoreCase("linechart")){
                int countChart=0;
                ((LineChartVH) holder).tvBotMessage.setText(models.get(position).getTitle());
                ((LineChartVH)holder).lineChart.setVisibility(View.VISIBLE);
                ArrayList<ILineDataSet> sets = new ArrayList<>();
                ArrayList<LineChartModel> lineChartModels=new ArrayList<>();
                for(BaseModel m : baseModels){
                    lineChartModels.add(new LineChartModel(m.getLabel(),m.getValue(),m.getValues(),m.getBody()));
                }
                for(LineChartModel lnModel : lineChartModels){
                    int [] values=lnModel.getValues();
                    countChart++;
                    ArrayList<Entry> entries=new ArrayList<>();
                    for(float i=0;i<(float)(values.length/10);i+=0.1){
                        entries.add(new Entry(i*10,(float)values[(int)(i*10)]));
                    }
                    LineDataSet d=new LineDataSet(entries,lnModel.getLabel());
                    d.setLineWidth(2.5f);
                    d.setCircleRadius(4.5f);
                    d.setHighLightColor(Color.rgb(244, 117, 117));
                    if(countChart<=5){
                        d.setColor(ColorTemplate.LIBERTY_COLORS[countChart-1]);
                        d.setCircleColor(ColorTemplate.LIBERTY_COLORS[countChart-1]);
                    }
                    else if( countChart>=5&&countChart<=10){
                        d.setColor(ColorTemplate.LIBERTY_COLORS[countChart-6]);
                        d.setCircleColor(ColorTemplate.LIBERTY_COLORS[countChart-6]);
                    }
                    d.setDrawValues(false);
                    sets.add(d);


                }
                LineData data=new LineData(sets);
                ((LineChartVH)holder).lineChart.setData(data);
            }
        }

    }
    public LineData getDataForChart(){
        ArrayList<Entry> keangNameValue=new ArrayList<>();
        for(int i=0;i<keangNam.length;i++){
            keangNameValue.add(new Entry(i+1,keangNam[i]));
        }
        ArrayList<Entry> thaiBinhValue=new ArrayList<>();
        for(int i=0;i<thaiBinh.length;i++){
            thaiBinhValue.add(new Entry(i+1,thaiBinh[i]));

        }
        LineDataSet set1,set2;
        set1=new LineDataSet(keangNameValue,"Bếp KeangNam");
        set2= new LineDataSet(thaiBinhValue,"Bếp Thái Bình");
        set1.setColor(ColorTemplate.getHoloBlue());
        set1.setCircleColor(Color.WHITE);
        set1.setLineWidth(2f);
        set1.setCircleRadius(3f);
        set1.setFillAlpha(65);
        set1.setFillColor(ColorTemplate.LIBERTY_COLORS[0]);
        set1.setHighLightColor(Color.rgb(244, 117, 117));
        set1.setDrawCircleHole(false);
      //  set2.setAxisDependency(YAxis.AxisDependency.RIGHT);
        set2.setColor(Color.RED);
        set2.setCircleColor(Color.WHITE);
        set2.setLineWidth(2f);
        set2.setCircleRadius(3f);
        set2.setFillAlpha(65);
        set2.setFillColor(ColorTemplate.MATERIAL_COLORS[0]);
        set2.setDrawCircleHole(false);
        set2.setHighLightColor(Color.rgb(244, 117, 117));
        ArrayList<ILineDataSet> iLineDataSets=new ArrayList<>();
        iLineDataSets.add(set1);
        iLineDataSets.add(set2);
        LineData data=new LineData(iLineDataSets);
        return data;
    }

    @Override
    public int getItemCount() {
        return models.size();
    }
}
