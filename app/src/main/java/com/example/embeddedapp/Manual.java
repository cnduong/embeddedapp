package com.example.embeddedapp;

import android.content.Intent;
import android.media.Image;

import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.appcompat.app.AppCompatActivity;

import com.example.embeddedapp.api.CarResponse;
import com.example.embeddedapp.api.controller.APICommon;
import com.example.embeddedapp.api.controller.ServiceGenerator;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Manual extends AppCompatActivity {

    private static final int LEFT = 5;
    private static final int RIGHT = 6;
    private static final int FORWARD = 3;
    private static final int BACKWARD = 4;
    private static final int STOP = 2;
    private static final int MANUAL = 1;
    private static final int AUTO = 0;
    private static final String WEB_SERVER = "http://192.168.43.207/mecha?arg=";

    private static final Long TIMEOUT = 15L;

    private OkHttpClient httpClient = new OkHttpClient.Builder()
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .build();

    private ImageButton btnLeft;
    private ImageButton btnRight;
    private ImageButton btnStop;
    private ImageButton btnForward;
    private ImageButton btnBackward;
    private ImageButton btnBack;
    private Switch switchManual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manual_layout);
        btnLeft = findViewById(R.id.btnLeft);
        btnRight = findViewById(R.id.btnRight);
        btnStop = findViewById(R.id.btnStop);
        btnBackward = findViewById(R.id.btnBackward);
        btnForward = findViewById(R.id.btnForward);
        switchManual = findViewById(R.id.switchManual);
        btnBack = findViewById(R.id.btnback);
        ButterKnife.bind(this);

        btnLeft.setOnClickListener(v -> btnLeftOnClickListener());
        btnRight.setOnClickListener(v -> btnRightOnClickListener());
        btnStop.setOnClickListener(v -> btnStopOnClickListener());
        btnForward.setOnClickListener(v -> btnForwardOnClickListener());
        btnBackward.setOnClickListener(v -> btnBackwardOnClickListener());
        switchManual.setOnClickListener(v -> switchManualOnClickListener());
    }

    private void switchManualOnClickListener() {
        if (switchManual.isChecked()) {
            new SendRequest(httpClient).execute(WEB_SERVER + MANUAL);
        } else {
            new SendRequest(httpClient).execute(WEB_SERVER + AUTO);
        }
    }

    @OnClick(R.id.btnback) public void backActivity(){
        Intent i = new Intent(this,MainActivity.class);
        startActivity(i);
    }

    private void btnBackwardOnClickListener() {
        sendData(BACKWARD+"");

    }

    private void btnForwardOnClickListener() {
        sendData(FORWARD+"");

    }

    private void btnStopOnClickListener() {
       sendData(STOP+"");
    }

    private void btnRightOnClickListener() {
       sendData(RIGHT+"");
    }

    private void btnLeftOnClickListener() {
        sendData(LEFT+"");

    }
    private void sendData(String key){
        final Call<CarResponse> drawRoute;
        APICommon.API service = ServiceGenerator.GetInstance("");
        drawRoute = service.getPlace(key);
        drawRoute.enqueue(new Callback<CarResponse>() {
            @Override
            public void onResponse(Call<CarResponse> call, Response<CarResponse> response) {
                System.out.println("thanh cong");
            }

            @Override
            public void onFailure(Call<CarResponse> call, Throwable t) {
                System.out.println("that bai");
            }
        });
    }
}