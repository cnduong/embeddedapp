package com.example.embeddedapp.Model.ChatBotModel;

public class Description {
    private String title;
    private String color;
    private int textSize;

    public Description(String title, String color, int textSize) {
        this.title = title;
        this.color = color;
        this.textSize = textSize;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getTextSize() {
        return textSize;
    }

    public void setTextSize(int textSize) {
        this.textSize = textSize;
    }
}
