package com.example.embeddedapp.Model.ChatBotModel;

public class MessageModel {
    private String message;
    private String color;
    private int size;

    public MessageModel(String message, String color, int size) {
        this.message = message;
        this.color = color;
        this.size = size;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
