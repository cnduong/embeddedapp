package com.example.embeddedapp.Model.ChatBotModel;

import java.util.ArrayList;

public class BarChartModel {
    String label;
    ArrayList<Integer> values;

    public BarChartModel(String label, ArrayList<Integer> values) {
        this.label = label;
        this.values = values;
    }

    public BarChartModel() {
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public ArrayList<Integer> getValues() {
        return values;
    }

    public void setValues(ArrayList<Integer> values) {
        this.values = values;
    }
}
