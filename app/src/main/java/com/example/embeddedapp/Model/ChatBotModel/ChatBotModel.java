package com.example.embeddedapp.Model.ChatBotModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatBotModel {
    @SerializedName("recipient_id")
    @Expose
    private String recipientId;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("type")
    @Expose
    private int type=0;
    public String getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(String recipientId) {
        this.recipientId = recipientId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ChatBotModel(String recipientId, String text) {
        this.recipientId = recipientId;
        this.text = text;
    }

    public ChatBotModel(String recipientId, String text, int type) {
        this.recipientId = recipientId;
        this.text = text;
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
