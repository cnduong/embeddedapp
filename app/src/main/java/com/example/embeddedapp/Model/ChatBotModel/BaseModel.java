package com.example.embeddedapp.Model.ChatBotModel;

public class BaseModel {
    public String label;
    public int value;
    public int [] values;
    Body body ;

    public BaseModel(String label, int value, int[] values,Body body) {
        this.label = label;
        this.value = value;
        this.values = values;
        this.body=body;

    }

    public BaseModel() {

    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int[] getValues() {
        return values;
    }

    public void setValues(int[] values) {
        this.values = values;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body bodyArrayList) {
        this.body = bodyArrayList;
    }
    //    public String getUrlImage() {
//        return urlImage;
//    }
//
//    public void setUrlImage(String urlImage) {
//        this.urlImage = urlImage;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getMessage() {
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
//
//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }
}
