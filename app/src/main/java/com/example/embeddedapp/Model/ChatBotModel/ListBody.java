package com.example.embeddedapp.Model.ChatBotModel;

public class ListBody {
    String imageView;
    NameModel nameModel;
    MessageModel messageModel;

    public ListBody(String imageView, NameModel nameModel, MessageModel messageModel) {
        this.imageView = imageView;
        this.nameModel = nameModel;
        this.messageModel = messageModel;
    }

    public String getImageView() {
        return imageView;
    }

    public void setImageView(String imageView) {
        this.imageView = imageView;
    }

    public NameModel getNameModel() {
        return nameModel;
    }

    public void setNameModel(NameModel nameModel) {
        this.nameModel = nameModel;
    }

    public MessageModel getMessageModel() {
        return messageModel;
    }

    public void setMessageModel(MessageModel messageModel) {
        this.messageModel = messageModel;
    }
}
