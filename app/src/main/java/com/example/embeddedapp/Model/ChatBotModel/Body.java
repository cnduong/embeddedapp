package com.example.embeddedapp.Model.ChatBotModel;

public class Body {
    private Header header;

    private ListBody listBody;
    private Description description;

    public Body(Header header, ListBody listBody, Description description) {
        this.header = header;
        this.listBody = listBody;
        this.description = description;
    }

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public ListBody getListBody() {
        return listBody;
    }

    public void setListBody(ListBody listBody) {
        this.listBody = listBody;
    }

    public Description getDescription() {
        return description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }
}
