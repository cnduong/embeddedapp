package com.example.embeddedapp.Model.ChatBotModel;

import java.util.ArrayList;

public class Model {
    public String header;
    public String type;
    public String data_type;
    public String title;
    public int columns;
    public String startDate;
    public String endDate;

    public ArrayList<BaseModel> baseModels;
    public ArrayList<ChatBotModel> chatBotModels;
    public Model() {
    }

    public Model(String header, String type, String data_type, String title, int columns, String startDate,
                 String endDate, ArrayList<BaseModel> baseModels, ArrayList<ChatBotModel> chatBotModels) {
        this.header = header;
        this.type = type;
        this.data_type = data_type;
        this.title = title;
        this.columns = columns;
        this.startDate = startDate;
        this.endDate = endDate;
        this.baseModels = baseModels;
        this.chatBotModels=chatBotModels;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getData_type() {
        return data_type;
    }

    public void setData_type(String data_type) {
        this.data_type = data_type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getColumns() {
        return columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public ArrayList<BaseModel> getBaseModels() {
        return baseModels;
    }

    public void setBaseModels(ArrayList<BaseModel> baseModels) {
        this.baseModels = baseModels;
    }

    public ArrayList<ChatBotModel> getChatBotModels() {
        return chatBotModels;
    }

    public void setChatBotModels(ArrayList<ChatBotModel> chatBotModels) {
        this.chatBotModels = chatBotModels;
    }
    //
//    public ArrayList<ListModel> listModels=new ArrayList<>();
//    public ArrayList<TableModel> tableModels=new ArrayList<>();
//    public ArrayList<PieChartModel> pieChartModels=new ArrayList<>();
//    public ArrayList<BarChartModel> barChartModels=new ArrayList<>();
//    public ArrayList<ChatBotModel> chatBotModels=new ArrayList<>();
//
//    public Model(String header, String type, String data_type, String title, int columns,
//                 String startDate, String endDate, ArrayList<ListModel> listModels,
//                 ArrayList<TableModel> tableModels, ArrayList<PieChartModel> pieChartModels
//            , ArrayList<BarChartModel> barChartModels, ArrayList<ChatBotModel> chatBotModels) {
//        this.header = header;
//        this.type = type;
//        this.data_type = data_type;
//        this.title = title;
//        this.columns = columns;
//        this.startDate = startDate;
//        this.endDate = endDate;
//        this.listModels = listModels;
//        this.tableModels = tableModels;
//        this.pieChartModels = pieChartModels;
//        this.barChartModels = barChartModels;
//        this.chatBotModels = chatBotModels;
//    }
//
//    public Model(String header, String type, String data_type, ArrayList<ListModel> listModels,
//                 ArrayList<TableModel> tableModels, ArrayList<PieChartModel> pieChartModels,
//                 ArrayList<ChatBotModel> chatBotModels) {
//        this.header = header;
//        this.type = type;
//        this.data_type = data_type;
//        this.listModels = listModels;
//        this.tableModels = tableModels;
//        this.pieChartModels = pieChartModels;
//        this.chatBotModels = chatBotModels;
//    }
//
//    public Model(String header, String type, String data_type, ArrayList<BarChartModel> barChartModels) {
//        this.header = header;
//        this.type = type;
//        this.data_type = data_type;
//        this.barChartModels = barChartModels;
//    }
//
//    public Model(String header, String type) {
//        this.header = header;
//        this.type = type;
//    }
//
//    public Model(String header) {
//        this.header = header;
//    }
//
//    public Model() {
//
//    }
//
//    public ArrayList<ChatBotModel> getChatBotModels() {
//        return chatBotModels;
//    }
//
//    public void setChatBotModels(ArrayList<ChatBotModel> chatBotModels) {
//        this.chatBotModels = chatBotModels;
//    }
//
//
//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
//    public int getColumns() {
//        return columns;
//    }
//
//    public void setColumns(int columns) {
//        this.columns = columns;
//    }
//
//    public String getStartDate() {
//        return startDate;
//    }
//
//    public void setStartDate(String startDate) {
//        this.startDate = startDate;
//    }
//
//    public String getEndDate() {
//        return endDate;
//    }
//
//    public void setEndDate(String endDate) {
//        this.endDate = endDate;
//    }
//
//    public String getType() {
//        return type;
//    }
//
//    public void setType(String type) {
//        this.type = type;
//    }
//
//    public String getData_type() {
//        return data_type;
//    }
//
//    public void setData_type(String data_type) {
//        this.data_type = data_type;
//    }
//
//    public String getHeader() {
//        return header;
//    }
//
//    public void setHeader(String header) {
//        this.header = header;
//    }
//
//    public ArrayList<ListModel> getListModels() {
//        return listModels;
//    }
//
//    public void setListModels(ArrayList<ListModel> listModels) {
//        this.listModels = listModels;
//    }
//
//    public ArrayList<TableModel> getTableModels() {
//        return tableModels;
//    }
//
//    public void setTableModels(ArrayList<TableModel> tableModels) {
//        this.tableModels = tableModels;
//    }
//
//    public ArrayList<BarChartModel> getBarChartModels() {
//        return barChartModels;
//    }
//
//    public void setBarChartModels(ArrayList<BarChartModel> barChartModels) {
//        this.barChartModels = barChartModels;
//    }
//
//    public ArrayList<PieChartModel> getPieChartModels() {
//        return pieChartModels;
//    }
//
//    public void setPieChartModels(ArrayList<PieChartModel> pieChartModels) {
//        this.pieChartModels = pieChartModels;
//    }
}
