package com.example.embeddedapp.Model.ChatBotModel;

public class Header {
    private String url;
    private String title;
    private String color;
    private int size;

    public Header(String url, String title, String color, int size) {
        this.url = url;
        this.title = title;
        this.color = color;
        this.size = size;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
