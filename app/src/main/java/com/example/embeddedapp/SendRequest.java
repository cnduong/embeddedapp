package com.example.embeddedapp;

import android.os.AsyncTask;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;

public class SendRequest extends AsyncTask<String, Void, Void> {

    private OkHttpClient httpClient;

    public SendRequest(OkHttpClient httpClient) {
        this.httpClient = httpClient;
    }

    @Override
    protected Void doInBackground(String... strings) {
        try {
            Request request = new  Request.Builder()
                    .url(strings[0])
                    .build();
            httpClient.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
