package com.example.embeddedapp.api.controller;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ServiceGenerator {
    public static APICommon.API GetInstance(String header){
        final Gson gson = new GsonBuilder().serializeNulls().create();
        Retrofit retrofit=null;


        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    Request original = chain.request();
                    Request request = original.newBuilder()
                            .addHeader("Accept", "application/json")
                            .method(original.method(), original.body())
                            .build();

                    return chain.proceed(request);
                });

            retrofit = new Retrofit.Builder()
                    .baseUrl(APICommon.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(clientBuilder.build())
                    .build();


        return retrofit.create(APICommon.API.class);
    }



}
