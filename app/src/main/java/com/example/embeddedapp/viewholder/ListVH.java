package com.example.embeddedapp.viewholder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.embeddedapp.R;


public class ListVH extends RecyclerView.ViewHolder {
    public TextView tvHeader;
    public RecyclerView rclListDanhSach;
    public ListVH(@NonNull View itemView) {
        super(itemView);
        rclListDanhSach=itemView.findViewById(R.id.rclListData);
        tvHeader=itemView.findViewById(R.id.tvHeader);
    }
}
