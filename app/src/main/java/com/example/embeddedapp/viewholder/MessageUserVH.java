package com.example.embeddedapp.viewholder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.embeddedapp.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class MessageUserVH extends RecyclerView.ViewHolder {
    public TextView tvUserMessage;
    public CircleImageView imgUser;
    public MessageUserVH(@NonNull View itemView) {
        super(itemView);
        tvUserMessage=itemView.findViewById(R.id.tv_message_user);
        imgUser=itemView.findViewById(R.id.imgUser);
    }
}
