package com.example.embeddedapp.viewholder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.embeddedapp.R;
import com.github.mikephil.charting.charts.LineChart;


public class MessageBotVH extends RecyclerView.ViewHolder {
    public TextView tvBotMessage;
    public LineChart lineChart;
    public MessageBotVH(@NonNull View itemView) {
        super(itemView);
        tvBotMessage=itemView.findViewById(R.id.tv_message_bot);
        lineChart=itemView.findViewById(R.id.lineChart);
    }
}
