package com.example.embeddedapp.viewholder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.embeddedapp.R;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;


public class LineChartVH  extends RecyclerView.ViewHolder {
    public TextView tvBotMessage;
    public LineChart lineChart;
    public LineChartVH(@NonNull View itemView) {
        super(itemView);
        tvBotMessage= itemView.findViewById(R.id.tv_message_bot);
        lineChart=itemView.findViewById(R.id.lineChart);
        lineChart.getDescription().setEnabled(false);
        lineChart.setMaxVisibleValueCount(60);
        lineChart.setPinchZoom(false);
        lineChart.setDrawGridBackground(false);
        lineChart.animateX(1500);
        lineChart.animateY(1500);
        lineChart.getLegend().setEnabled(true);// hien thi lineChart

        XAxis xAxis=lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        lineChart.getAxisRight().setEnabled(false);
        Legend legend = lineChart.getLegend();
        legend.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        legend.setOrientation(Legend.LegendOrientation.VERTICAL);
        legend.setDrawInside(false);
        legend.setYOffset(0f);
        legend.setXOffset(10f);
        legend.setYEntrySpace(0f);
        legend.setTextSize(8f);
    }
}
