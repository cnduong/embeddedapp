package com.example.embeddedapp.viewholder;

import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.embeddedapp.R;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;

public class PieChartVH extends RecyclerView.ViewHolder {
    public TextView tvBotMessage;
    public PieChart pieChart;
    public PieChartVH(@NonNull View itemView) {
        super(itemView);
        tvBotMessage=itemView.findViewById(R.id.tv_message_bot);
        pieChart=itemView.findViewById(R.id.pieChart);
        pieChart.setUsePercentValues(true);
        pieChart.getDescription().setEnabled(false);
        pieChart.setExtraOffsets(5, 10, 5, 5);
//        Legend l = pieChart.getLegend(); // get legend of pie
//        l.setVerticalAlignment(Legend.LegendVerticalAlignment.CENTER); // set vertical alignment for legend
//       // l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT); // set horizontal alignment for legend
//   //     l.setOrientation(Legend.LegendOrientation.VERTICAL); // set orientation for legend
//        l.setDrawInside(false); // set if legend should be drawn inside or not

        pieChart.setDrawMarkers(false);
        pieChart.setDragDecelerationFrictionCoef(0.95f);
        pieChart.setDrawEntryLabels(false);//label
        //    pieChart.setCenterTextTypeface(tfLight);
        //   pieChart.setCenterText(generateCenterSpannableText());

        pieChart.setDrawHoleEnabled(false);
        pieChart.setHoleColor(Color.WHITE);

        pieChart.setTransparentCircleColor(Color.BLACK);
        pieChart.setTransparentCircleAlpha(110);

        pieChart.setHoleRadius(58f);
        pieChart.setTransparentCircleRadius(61f);

        pieChart.setDrawCenterText(true);

        pieChart.setRotationAngle(0);
        // enable rotation of the pieChart by touch
        pieChart.setRotationEnabled(true);
        pieChart.setHighlightPerTapEnabled(true);

        // pieChart.setUnit(" €");
        // pieChart.setDrawUnitsInChart(true);

        // add a selection listener




        pieChart.animateY(1400, Easing.EaseInOutQuad);
        Legend l = pieChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);
        l.setEnabled(true);
        // entry label styling
        pieChart.setEntryLabelColor(Color.WHITE);

        pieChart.setEntryLabelTextSize(12f);
        // pieChart.spin(2000, 0, 360);
    }
}
