package com.example.embeddedapp;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.embeddedapp.Adapter.AdapterBot;
import com.example.embeddedapp.Model.ChatBotModel.ChatBotModel;
import com.example.embeddedapp.Model.ChatBotModel.Model;
import com.example.embeddedapp.api.CarResponse;
import com.example.embeddedapp.api.controller.APICommon;
import com.example.embeddedapp.api.controller.ServiceGenerator;
import com.github.zagum.speechrecognitionview.RecognitionProgressView;
import com.github.zagum.speechrecognitionview.adapters.RecognitionListenerAdapter;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatBotActivity extends AppCompatActivity {

    @BindView(R.id.btnMic)
    ImageView btnMic;
    @BindView(R.id.rcl_comunication)
    RecyclerView rclComunication;

    @BindView(R.id.btnback)
    ImageButton btnBack;


    @BindView((R.id.recognition_view))
    RecognitionProgressView recognitionProgressView;


    ArrayList<Model> listModel;
    SpeechRecognizer speechRecognizer;
    Intent recognizerIntent;
    public final int REQUEST_RECORD_PERMISSION=100;
    Boolean isRecord=false;
    //ChatBotAdapter chatBotAdapter;

    AdapterBot adapterBot;

    private static final int LEFT = 5;
    private static final int RIGHT = 6;
    private static final int FORWARD = 3;
    private static final int BACKWARD = 4;
    private static final int STOP = 2;
    private static final int MANUAL = 1;
    private static final int AUTO = 0;
    private static final String WEB_SERVER = "http://192.168.43.207/mecha?arg=";

    private static final Long TIMEOUT = 15L;

    private OkHttpClient httpClient = new OkHttpClient.Builder()
            .readTimeout(TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
            .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
            .build();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatbot);
        ButterKnife.bind(this);
        //  initView();
        initData();
        requestPermsission();
        //  checkPermission();
    }



    private void requestPermsission(){
        int hasPermission= ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        if(hasPermission== PackageManager.PERMISSION_DENIED){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.RECORD_AUDIO},REQUEST_RECORD_PERMISSION);

        }
    }
    private void initData(){
        listModel=new ArrayList<>();
        speechRecognizer=SpeechRecognizer.createSpeechRecognizer(this);


        int[] colors = {
                ContextCompat.getColor(this, R.color.color1),
                ContextCompat.getColor(this, R.color.color2),
                ContextCompat.getColor(this, R.color.color3),
                ContextCompat.getColor(this, R.color.color4),
                ContextCompat.getColor(this, R.color.color5)
        };

        int[] heights = { 20, 24, 18, 23, 16 };




        recognitionProgressView.setSpeechRecognizer(speechRecognizer);
        recognitionProgressView.setRecognitionListener(new RecognitionListenerAdapter() {
            @Override
            public void onReadyForSpeech(Bundle params) {
                super.onReadyForSpeech(params);
            }


            @Override
            public void onResults(Bundle results) {
                showResults(results);
                showLog("Result");
            }

            @Override
            public void onError(int error) {
                super.onError(error);
                errorRecognition(error);
            }

            @Override
            public void onEndOfSpeech() {
                super.onEndOfSpeech();
                endRecognition();
            }

            @Override
            public void onBeginningOfSpeech() {
                super.onBeginningOfSpeech();
                isRecord=true;
                showLog("Beginning");
            }

        });

//        listMessage=new ArrayList<ChatBotModel>();
//        listMessage.add(new ChatBotModel("vtnet-bot","Xin chào"+", tôi có thể giúp gì cho bạn ?",0));
        //chatBotAdapter=new ChatBotAdapter(getApplicationContext(),listModel);
        adapterBot=new AdapterBot(getApplicationContext(),listModel);
        ArrayList<ChatBotModel> listMessage=new ArrayList<>();
        listMessage.add(new ChatBotModel("cao nam duong","Xin chào , tôi có thể giúp gì cho bạn",0));
        Model model= new Model("","text","bot",null,0,null,null,null,listMessage);

        listModel.add(model);

        adapterBot.notifyDataSetChanged();
        rclComunication.setAdapter(adapterBot);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        rclComunication.setLayoutManager(linearLayoutManager);
        recognitionProgressView.setColors(colors);
        recognitionProgressView.setBarMaxHeightsInDp(heights);
        recognitionProgressView.setCircleRadiusInDp(2);
        recognitionProgressView.setSpacingInDp(2);
        recognitionProgressView.setIdleStateAmplitudeInDp(2);
        recognitionProgressView.setRotationRadiusInDp(10);
        recognitionProgressView.play();


    }
    public void showResults(Bundle results){
        ArrayList<String> result =results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

        ChatBotModel chatBotModel=new ChatBotModel("Cao Nam Dương",result.get(0),1);
        ArrayList<ChatBotModel> chatBotModels=new ArrayList<>();
        chatBotModels.add(chatBotModel);
        Model model=new Model("","text","user",null,0,null,null,null,chatBotModels);
        listModel.add(model);
        adapterBot.notifyDataSetChanged();

        rclComunication.scrollToPosition(adapterBot.getItemCount()-1);

        String dulieutext = result.get(0);

        if (dulieutext.equals("rẽ trái") || dulieutext.equals("sang trái") || dulieutext.equals("quay trái")||dulieutext.equals("trái")||dulieutext.equals("đi trái")){
            sendData(LEFT+"");
        }
        else if (dulieutext.equals("rẽ phải") || dulieutext.equals("sang phải") || dulieutext.equals("quay phải")||dulieutext.equals("phải")||dulieutext.equals("đi phải")){
            sendData(RIGHT+"");
        }
        else if (dulieutext.equals("đi thẳng") || dulieutext.equals("thẳng") || dulieutext.equals("tiến thẳng")||dulieutext.equals("tiến lên")||dulieutext.equals("lên")){
            sendData(FORWARD+"");
        }
        else if (dulieutext.equals("đi xuống") || dulieutext.equals("xuống") || dulieutext.equals("đi lùi")||dulieutext.equals("lùi")){
            sendData(BACKWARD+"");
        }
        else if (dulieutext.equals("dừng lại") || dulieutext.equals("dừng") || dulieutext.equals("đứng ")||dulieutext.equals("")){
            sendData(STOP+"");
        }

        Handler handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                btnMic.setVisibility(View.VISIBLE);
                recognitionProgressView.setVisibility(View.INVISIBLE);
                resetAnim();

            }
        },1000);

        showLog("da co kq");
        isRecord=false;

    }

    private void sendData(String key){
        final Call<CarResponse> drawRoute;
        APICommon.API service = ServiceGenerator.GetInstance("");
        drawRoute = service.getPlace(key);
        drawRoute.enqueue(new Callback<CarResponse>() {
            @Override
            public void onResponse(Call<CarResponse> call, Response<CarResponse> response) {
                System.out.println("thanh cong");
            }

            @Override
            public void onFailure(Call<CarResponse> call, Throwable t) {
                System.out.println("that bai");
            }
        });
    }


    public void errorRecognition(int i){
        resetAnim();
        btnMic.setVisibility(View.VISIBLE);
        recognitionProgressView.setVisibility(View.INVISIBLE);
        switch (i){
            case SpeechRecognizer.ERROR_AUDIO:{ // lỗi audio
                showLog("audio error");
                break;
            }
            case SpeechRecognizer.ERROR_CLIENT:{ // lỗi client
                showLog("ERROR CLIENT");
                if(speechRecognizer!=null){
                    speechRecognizer.stopListening();
                    speechRecognizer.cancel();
                }

                isRecord=false;

                return;
            }
            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:{// recognizer busy
                showLog("ERROR_RECOGNIZER_BUSY");

                break;
            }
            case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:// permission_ khon can thiet
                showLog("ERROR_INSUFFICIENT_PERMISSIONS");
                break;
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT: // het gio
                showLog("ERROR_NETWORK_TIMEOUT");
                break;
            case SpeechRecognizer.ERROR_NETWORK: //mang
                showLog("ERROR_NETWORK");
                break;
            case SpeechRecognizer.ERROR_SERVER://server
                showLog("ERROR_SERVER");
                break;
            case SpeechRecognizer.ERROR_NO_MATCH: // k tim thay
                showLog("ERROR_NO_MATCH");
                ArrayList<ChatBotModel>chatBotModels=new ArrayList<>();

                chatBotModels.add(new ChatBotModel("vtnet-bot","Xin lỗi tôi không hiểu bạn nói gì ....",0));
                Model model= new Model("","text","bot",null,0,null,null,null,chatBotModels);
                listModel.add(model);
                adapterBot.notifyDataSetChanged();
                rclComunication.scrollToPosition(adapterBot.getItemCount()-1);
                if(speechRecognizer!=null) speechRecognizer.destroy();

                return;
            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT: // het thoi gian noi
                showLog("ERROR_SPEECH_TIMEOUT");
                break;
            default:
                return;

        }
        isRecord=false;
        if(speechRecognizer!=null){
            speechRecognizer.stopListening();
            speechRecognizer.cancel();
        }

    }
    public void endRecognition(){
        showLog("end");
        isRecord=false;

        speechRecognizer.stopListening();
    }

    public <T> ArrayList<T> generic(ArrayList<Object> objs){
        ArrayList<T> tArrayList=new ArrayList<>();
        for(Object o: objs){
            T t=(T) o;
            tArrayList.add(t);
        }
        return tArrayList;

    }
    public void startRecogni(){
        recognizerIntent=new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "vi_VN");
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE,"vi_VN");
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS,1);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,this.getPackageName());
        if(speechRecognizer==null) speechRecognizer=SpeechRecognizer.createSpeechRecognizer(this);
        recognitionProgressView.setSpeechRecognizer(speechRecognizer);
        speechRecognizer.startListening(recognizerIntent);
    }

    @OnClick(R.id.btnback) public void backActivity(){
        Intent i = new Intent(this,MainActivity.class);
        startActivity(i);
    }
    @OnClick(R.id.btnMic) public void startRecognition(){
        int hasPermission= ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        if(hasPermission== PackageManager.PERMISSION_GRANTED){


            if(!isRecord){
                btnMic.setVisibility(View.INVISIBLE);
                recognitionProgressView.setVisibility(View.VISIBLE);
                isRecord=true;
                showLog("start Listenning");
                recognitionProgressView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startRecogni();
                    }
                }, 50);
            }
            else {
                isRecord=false;
                btnMic.setVisibility(View.VISIBLE);
                recognitionProgressView.setVisibility(View.INVISIBLE);

                if(speechRecognizer!=null) speechRecognizer.destroy();
                resetAnim();

                showLog("stop Listenning");
            }

        }
        else{
            AlertDialog.Builder builder=new AlertDialog.Builder(this);
            builder.setCancelable(false);
            builder.setMessage(R.string.question_permission)
                    .setPositiveButton(R.string.accept_permission, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            requestPermsission();
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton(R.string.cancel_permission, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            AlertDialog alertDialog=builder.create();
            alertDialog.show();
        }
    }
    @OnClick(R.id.recognition_view) public void cancelService(){
        isRecord=false;
        btnMic.setVisibility(View.VISIBLE);
        recognitionProgressView.setVisibility(View.INVISIBLE);
        if(speechRecognizer!=null) speechRecognizer.destroy();
        resetAnim();
        showLog("stop Listenning");
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case   REQUEST_RECORD_PERMISSION:{
                if(grantResults.length>0&&grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(this,"Ghi âm đã sẵn sàng ",Toast.LENGTH_SHORT).show();
                }
                else{
                    showLog("Người dùng không cấp quyền cho ứng dụng");
                }
            }
        }
    }
    private void resetAnim(){
        recognitionProgressView.stop();
        recognitionProgressView.play();
    }
    private void showLog(String log){
        Log.d("SpeechToText",log);
    }


    @Override
    protected void onPause() {

        Log.i("speech to text", "on pause called");
        if(speechRecognizer!=null){
            speechRecognizer.stopListening();
            speechRecognizer.cancel();
            //     speechRecognizer.destroy();

        }
        speechRecognizer = null;

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(speechRecognizer!=null) {

            speechRecognizer.destroy();
        }

    }
}
