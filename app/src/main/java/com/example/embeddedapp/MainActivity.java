package com.example.embeddedapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnAuto,btnSpeech;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnAuto = (Button) findViewById(R.id.byAuto);
        btnSpeech = (Button) findViewById(R.id.bySpeech);
        btnSpeech.setOnClickListener(this);
        btnAuto.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.bySpeech:{
                Intent i = new Intent(this,ChatBotActivity.class);
                startActivity(i);
                break;
            }
            case R.id.byAuto :{
                Intent i = new Intent(this,Manual.class);
                startActivity(i);
                break;
            }
        }
    }
}
